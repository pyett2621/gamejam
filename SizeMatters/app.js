/// <reference path="phaser.d.ts" />
var levelGlob;
var playersGlob;
var SimpleGame = (function () {
    function SimpleGame() {
        this.players = new Array();
        this.game = new Phaser.Game(1920, 1088, Phaser.AUTO, 'content', { preload: this.preload, create: this.create, update: this.update, render: this.render });
    }
    SimpleGame.prototype.preload = function () {
        //this.game.load.tilemap('map', 'images/map.json', null, Phaser.Tilemap.TILED_JSON);
        //this.game.load.image('tileset', 'images/tileset.png');
        //code for creating all the current maps
        this.game.load.tilemap('BattleField', 'images/json/BattleField.json', null, Phaser.Tilemap.TILED_JSON);
        this.game.load.tilemap('Boat', 'images/json/Boat.json', null, Phaser.Tilemap.TILED_JSON);
        this.game.load.tilemap('Boat2', 'images/json/Boat2.json', null, Phaser.Tilemap.TILED_JSON);
        this.game.load.tilemap('House', 'images/json/House.json', null, Phaser.Tilemap.TILED_JSON);
        this.game.load.tilemap('Industrial', 'images/json/Industrial.json', null, Phaser.Tilemap.TILED_JSON);
        this.game.load.tilemap('Platform', 'images/json/Platform.json', null, Phaser.Tilemap.TILED_JSON);
        this.game.load.tilemap('SnowDay', 'images/json/SnowDay.json', null, Phaser.Tilemap.TILED_JSON);
        //code for loading all current tilemaps
        this.game.load.image('GoodTiles', 'images/tile/GoodTiles.png');
        this.game.load.image('MyTiles', 'images/tile/MyTiles.png');
        this.game.load.image('SunPicture', 'images/tile/SunPicture.png');
        this.game.load.image('TileSheet2', 'images/tile/TileSheet2.png');
        //loading other images
        this.game.load.image('man', 'images/man.png');
        this.game.load.image('body', 'images/Body.png');
        this.game.load.image('head', 'images/Head-02.png');
        this.game.load.image('limb', 'images/Limb-03.png');
        this.game.load.image('broom', 'images/broom.png');
        this.game.load.image('machete', 'images/machete.png');
        this.game.load.image('playerparent', 'images/playerparent.png');
        this.game.load.image('blob', 'images/Limb-03.png');
        this.game.load.image('grenade', 'images/grenade.png');
        this.game.load.image('mine', 'images/mine.png');
        this.game.load.spritesheet('explosion', 'images/expl.png', 153, 146);
        this.game.load.spritesheet('knife', 'images/knife.png', 128, 40);
        //Loading boat ladder images for sprites
        this.game.load.image('BoatLadderLeft', 'images/tile/BoatLadderLeft.png');
        this.game.load.image('BoatLadderRight', 'images/tile/BoatLadderRight.png');
        this.game.load.image('HouseLadder', 'images/tile/HouseLadder.png');
        this.game.load.image('IndustrialLadder', 'images/tile/IndustrialLadder.png');
        this.game.load.image('IndustrialLadderTop', 'images/tile/IndustrialLadderTop.png');
        //Sounds
        //music
        this.game.load.audio('bg1', 'audio/battle.wav');
        this.game.load.audio('bg2', 'audio/loading.wav');
        this.game.load.audio('bg3', 'audio/meet-the-princess.wav');
        this.game.load.audio('bg4', 'audio/slow-travel.wav');
        //sfx
        this.game.load.audio('blob', 'audio/bubble1.mp3');
        this.game.load.audio('whoosh', 'audio/whoosh.mp3');
        this.game.load.audio('explosion', 'audio/explosion.mp3');
        this.blobconst = 200;
        this.blobcooldown = this.blobconst;
        this.numPlayers = 4;
    };
    SimpleGame.prototype.create = function () {
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        var mus = Math.floor(Math.random() * 4 + 1);
        this.music = this.game.add.audio('bg' + mus);
        this.music.loop = true;
        this.music.play();
        //this.game.stage.backgroundColor = '#a9f0ff';
        //this is an array that will hold the names of our maps, change the number of the map you want to 
        //the corresponding array value to change the map.
        //set up shit accordingly so that it can use the right shit
        this.mapArray = ['BattleField', 'Boat2', 'House', 'Industrial', 'Platform', 'SnowDay'];
        this.currentMap = Math.floor(Math.random() * 6);
        var map = this.game.add.tilemap(this.mapArray[this.currentMap]);
        //code to create all of the current maps ****depricated because of map array*****
        //var map = this.game.add.tilemap('BattleField');
        //var map = this.game.add.tilemap('Boat');
        //var map = this.game.add.tilemap('House');
        //var map = this.game.add.tilemap('Industrial');
        //var map = this.game.add.tilemap('Platform');
        //var map = this.game.add.tilemap('SnowDay');
        //code to load all the tilesets
        map.addTilesetImage('GoodTiles', 'GoodTiles');
        map.addTilesetImage('MyTiles', 'MyTiles');
        map.addTilesetImage('SunPicture', 'SunPicture');
        map.addTilesetImage('TileSheet2', 'TileSheet2');
        //code to create different layers from amp
        this.backgroundLayer = map.createLayer('BackgroundLayer');
        this.groundLayer = map.createLayer('GroundLayer');
        this.ladderLayer = map.createLayer('LadderLayer');
        this.fallThroughPlatfromLayer = map.createLayer('FallThroughPlatformLayer');
        this.ladderTopLayer = map.createLayer('LadderTopLayer');
        //this if statement checks what map is being used, and runs the neccessary methods for that map.
        //1 is boat, 2 is house, and 3 is industrial
        this.WeaponSpawnLayer = this.game.add.group();
        this.PlayerSpawnLayer = this.game.add.group();
        switch (this.currentMap) {
            case 1:
                createLadders(map, this);
                map.setCollisionBetween(1, 2500, true, 'FallThroughPlatformLayer');
                break;
            case 2:
                createLaddersHouse(map, this);
                break;
            case 3:
                createLaddersIndustrial(map, this);
                break;
        }
        createWeaponSpawns(map, this);
        var playerSpawnArray = createPlayerSpawns(map, this);
        console.log(playerSpawnArray);
        map.setCollisionBetween(1, 2500, true, 'GroundLayer');
        this.groundLayer.resizeWorld();
        this.blobs = new Array();
        this.grenades = new Array();
        this.explosions = this.game.add.group();
        this.mines = new Array();
        this.players = new Array();
        this.pads = new Array();
        var tint;
        this.game.input.gamepad.start();
        //register all 4 gamepads
        this.pads[0] = this.game.input.gamepad.pad1;
        this.pads[1] = this.game.input.gamepad.pad2;
        this.pads[2] = this.game.input.gamepad.pad3;
        this.pads[3] = this.game.input.gamepad.pad4;
        for (var i = 0; i < this.numPlayers; i++) {
            switch (i) {
                case 0:
                    tint = 0xff0000;
                    break;
                case 1:
                    tint = 0x00ff00;
                    break;
                case 2:
                    tint = 0x0000ff;
                    break;
                case 3:
                    tint = 0x00ffff;
                    break;
            }
            //SPRITE MAKING BELOW
            //this.players[i] = this.game.add.sprite(this.game.world.width / 2, this.game.world.height / 2, 'body');
            this.players[i] = this.game.add.sprite(playerSpawnArray[i].x, playerSpawnArray[i].y, 'body');
            this.players[i].mine = true;
            this.players[i].grenade = true;
            this.players[i].area = 50.27; //initial "area" of a sprite. This determines size
            this.players[i].speed = 300;
            this.players[i].tint = tint;
            this.players[i].scale.setTo(Math.sqrt(this.players[i].area / Math.PI) / 100, Math.sqrt(this.players[i] / Math.PI) / 100);
            this.game.physics.arcade.enable(this.players[i]);
            this.players[i].body.gravity.y = 300;
            this.players[i].body.collideWorldBounds = true;
            this.players[i].anchor.x = 0.5;
            this.players[i].anchor.y = 1;
            //head
            this.players[i].head = this.game.add.sprite(0, 0, 'head');
            this.game.physics.arcade.enable(this.players[i].head);
            console.log(this.players[i].head.body.halfWidth);
            this.players[i].ghosthead = this.game.add.sprite(0, 0, null);
            this.players[i].addChild(this.players[i].ghosthead);
            this.players[i].head.scale.setTo(0.07, 0.07);
            this.players[i].head.tint = tint;
            //left foot
            this.players[i].ghostleftfoot = this.game.add.sprite(0, 0, null);
            this.players[i].addChild(this.players[i].ghostleftfoot);
            this.players[i].leftfoot = this.game.add.sprite(0, 0, 'limb');
            this.players[i].leftfoot.scale.setTo(0.07, 0.07);
            this.players[i].leftfoot.tint = tint;
            this.game.physics.arcade.enable(this.players[i].leftfoot);
            //this.players[i].leftfoot.body.gravity.y = 500;
            //right foot
            this.players[i].ghostrightfoot = this.game.add.sprite(0, 0, null);
            this.players[i].addChild(this.players[i].ghostrightfoot);
            this.players[i].rightfoot = this.game.add.sprite(0, 0, 'limb');
            this.players[i].rightfoot.scale.setTo(0.07, 0.07);
            this.players[i].rightfoot.tint = tint;
            this.game.physics.arcade.enable(this.players[i].rightfoot);
            //this.players[i].rightfoot.body.gravity.y = 500;
            //left hand
            this.players[i].ghostlefthand = this.game.add.sprite(0, 0, null);
            this.players[i].addChild(this.players[i].ghostlefthand);
            this.players[i].lefthand = this.game.add.sprite(0, 0, 'limb');
            this.players[i].lefthand.scale.setTo(0.07, 0.07);
            this.players[i].lefthand.tint = tint;
            this.game.physics.arcade.enable(this.players[i].lefthand);
            //right hand
            this.players[i].righthand = this.game.add.sprite(0, 0, 'limb');
            this.players[i].ghostrighthand = this.game.add.sprite(0, 0, null);
            this.players[i].addChild(this.players[i].ghostrighthand);
            this.players[i].righthand.scale.setTo(0.07, 0.07);
            this.players[i].righthand.tint = tint;
            this.game.physics.arcade.enable(this.players[i].righthand);
            console.log(this.players[i].scale);
            //jump sprite
            this.players[i].jumper = this.game.add.sprite(0, 0);
            this.game.physics.arcade.enable(this.players[i].jumper);
            this.players[i].jumper.height = this.players[i].leftfoot.body.height * 2;
            this.players[i].leftHold = true;
            this.players[i].motion = 0;
            //***************************
            this.players[i].name = 'Player ' + (i + 1);
            //***************************            //gamepads
            this.players[i].mine = true;
            this.players[i].grenade = true;
        }
        this.WorldWeapons = this.game.add.group();
        this.WeaponSpawnLayer.forEach(function (child) {
            var weapon = undefined;
            var weaponNum = Math.floor(Math.random() * (6 - 1) + 1);
            //console.log('weapon ' + weaponNum + ' generated');
            switch (weaponNum) {
                case 1:
                    weapon = this.WorldWeapons.create(child.x + 75, child.y, 'machete');
                    weapon.anchor.setTo(1, 0.5);
                    this.game.physics.arcade.enable(weapon);
                    break;
                case 2:
                    weapon = this.WorldWeapons.create(child.x + 75, child.y, 'broom');
                    weapon.scale.setTo(0.5, 0.5);
                    weapon.angle = 90;
                    weapon.anchor.setTo(0.75, 0);
                    this.game.physics.arcade.enable(weapon);
                    break;
                case 3:
                    weapon = this.WorldWeapons.create(child.x + 64, child.y, 'knife');
                    weapon.scale.setTo(0.5, 0.5);
                    weapon.anchor.setTo(1, 0.5);
                    weapon.animations.add('hold', [1], 1, true);
                    weapon.animations.add('stab', [2, 0], 20, false);
                    this.game.physics.arcade.enable(weapon);
                    break;
                case 4:
                    weapon = this.WorldWeapons.create(child.x, child.y, 'grenade');
                    weapon.anchor.setTo(0.5, 1);
                    this.game.physics.arcade.enable(weapon);
                    break;
                case 5:
                    weapon = this.WorldWeapons.create(child.x, child.y, 'mine');
                    weapon.anchor.setTo(0.5, 1);
                    this.game.physics.arcade.enable(weapon);
                    break;
            }
            child.holdingWeapon = true;
            weapon.location = child;
        }, this);
        this.game.input.gamepad.start();
        //this.pads[0] = this.game.input.gamepad.pad1;
        this.pads[0].addCallbacks(this, {
            onConnect: function () {
                addButtons(this.pads[0], this.players, 0, this);
            },
            onAxis: function () {
                moveAxis(this.pads[0], this.players[0], this);
            }
        });
        this.pads[1].addCallbacks(this, {
            onConnect: function () {
                addButtons(this.pads[1], this.players, 1, this);
            },
            onAxis: function () {
                moveAxis(this.pads[1], this.players[1], this);
            }
        });
        this.pads[2].addCallbacks(this, {
            onConnect: function () {
                addButtons(this.pads[2], this.players, 2, this);
            },
            onAxis: function () {
                moveAxis(this.pads[2], this.players[2], this);
            }
        });
        this.pads[3].addCallbacks(this, {
            onConnect: function () {
                addButtons(this.pads[3], this.players, 3, this);
            },
            onAxis: function () {
                moveAxis(this.pads[3], this.players[3], this);
            }
        });
    };
    SimpleGame.prototype.update = function () {
        //it's raining blobs, hallelujah
        if (this.blobcooldown > 0) {
            this.blobcooldown--;
        }
        else {
            var rid = 0;
            if (this.blobs.length > 50) {
                rid = 1;
            }
            for (var j = 0; j < rid; j++) {
                this.blobs[j].destroy();
                this.blobs.splice(j, 1);
            }
            var blob = this.game.add.audio('blob');
            blob.play();
            this.blobs.push(this.game.add.sprite(Math.random() * this.game.width, Math.random() * this.game.height / 2, 'blob'));
            this.blobs[this.blobs.length - 1].scale.setTo(0.12, 0.12);
            this.game.physics.arcade.enable(this.blobs[this.blobs.length - 1]);
            this.blobs[this.blobs.length - 1].body.gravity.y = 300;
            this.blobs[this.blobs.length - 1].tint = 0xffffff;
            this.blobs[this.blobs.length - 1].data.tintcount = 0;
            this.blobcooldown = this.blobconst;
            console.log(this.blobs);
        }
        //blobs countdown
        for (var i = 0; i < this.blobs.length; i++) {
            this.game.physics.arcade.collide(this.blobs[i], this.groundLayer, function (blob, layer) {
                blob.body.velocity.x = 0;
                if (blob.data.tintcount > 0) {
                    blob.data.tintcount--;
                }
                else {
                    blob.tint = 0xffffff;
                }
            });
        }
        //grenade countdown
        for (var i = 0; i < this.grenades.length; i++) {
            this.game.physics.arcade.collide(this.grenades[i], this.groundLayer);
            if (this.grenades[i].body.velocity.x < -1.5) {
                this.grenades[i].body.velocity.x += 1.5;
            }
            else if (this.grenades[i].body.velocity.x > 1.5) {
                this.grenades[i].body.velocity.x -= 1.5;
            }
            else {
                this.grenades[i].body.velocity.x = 0;
            }
            if (this.grenades[i].data.countdown > 0) {
                this.grenades[i].data.countdown--;
            }
            else {
                addBoom(this.grenades[i], this, this.grenades);
                //this.explosions.push(this.game.add.sprite(this.grenades[i].world.x, this.grenades[i].world.y, 'explosion'));
                //this.game.physics.arcade.enable(this.explosions[this.explosions.length - 1]);
                //this.explosions[this.explosions.length - 1].anchor.setTo(0.5, .85);
                //this.grenades[i].destroy();
                //this.grenades.splice(i, 1);
                //this.explosions[this.explosions.length - 1].animations.add('boomgren' + i, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14], 30);
                //this.explosions[this.explosions.length - 1].animations.play('boomgren' + i, 30, 0, true);
                //this.game.physics.arcade.enable(this.explosions[this.explosions.length - 1]);
                //this.explosions[this.explosions.length - 1].data.countdown = 300;
            }
        }
        //mine living
        for (var i = 0; i < this.mines.length; i++) {
            this.game.physics.arcade.collide(this.mines[i], this.groundLayer, function (mine, layer) {
                mine.body.velocity.x = 0;
                mine.data.live = true;
            });
        }
        for (var i = 0; i < this.numPlayers; i++) {
            this.players[i].head.body.x = this.players[i].ghosthead.world.x - this.players[i].head.body.halfWidth;
            this.players[i].head.body.y = this.players[i].ghosthead.world.y - this.players[i].body.height - (this.players[i].head.body.height * 1.25);
            this.players[i].leftfoot.body.x = this.players[i].ghostleftfoot.world.x - (this.players[i].leftfoot.body.halfWidth * 3);
            this.players[i].leftfoot.body.y = this.players[i].ghostleftfoot.world.y + (this.players[i].leftfoot.body.halfWidth);
            this.players[i].jumper.body.x = this.players[i].ghostleftfoot.world.x;
            this.players[i].jumper.body.y = this.players[i].ghostleftfoot.world.y;
            this.players[i].rightfoot.body.x = this.players[i].ghostrightfoot.world.x + (this.players[i].rightfoot.body.halfWidth);
            this.players[i].rightfoot.body.y = this.players[i].ghostrightfoot.world.y + (this.players[i].rightfoot.body.halfWidth);
            this.players[i].lefthand.body.x = this.players[i].ghostlefthand.world.x - this.players[i].body.halfWidth - (this.players[i].lefthand.body.width * .25) - this.players[i].lefthand.body.width;
            this.players[i].lefthand.body.y = this.players[i].ghostlefthand.world.y - (this.players[i].body.width * 0.8);
            this.players[i].righthand.body.x = this.players[i].ghostrighthand.world.x + this.players[i].body.halfWidth + (this.players[i].lefthand.body.width * .25);
            this.players[i].righthand.body.y = this.players[i].ghostrighthand.world.y - (this.players[i].body.width * 0.8);
            if (this.players[i].weapon) {
                this.players[i].weapon.body.y = this.players[i].righthand.world.y;
                if (this.players[i].leftHold) {
                    this.players[i].weapon.body.x = this.players[i].lefthand.world.x; // - this.players[i].weapon.body.width;
                }
                else {
                    this.players[i].weapon.body.x = this.players[i].righthand.world.x + this.players[i].lefthand.body.width;
                }
                switch (this.players[i].weapon.key) {
                    case 'machete':
                        if (this.players[i].leftHold) {
                            this.players[i].weapon.body.x -= this.players[i].weapon.body.width;
                        }
                        else {
                            //this.players[i].weapon.body.x += this.players[i].weapon.body.width;
                        }
                        break;
                    case 'broom':
                        if (this.players[i].leftHold) {
                            this.players[i].weapon.body.x -= this.players[i].weapon.body.width / 4 * 3;
                        }
                        else {
                            this.players[i].weapon.body.x -= this.players[i].weapon.body.width / 4;
                        }
                        break;
                    case 'knife':
                        if (this.players[i].leftHold) {
                            this.players[i].weapon.body.x -= this.players[i].weapon.body.width;
                        }
                        else {
                            //this.players[i].weapon.body.x += this.players[i].weapon.body.width;
                        }
                        break;
                }
            }
            this.game.physics.arcade.collide(this.players[i].leftfoot, this.groundLayer);
            this.game.physics.arcade.collide(this.players[i].jumper, this.groundLayer, function (jumper, layer) { this.players[i].jumpable = true; }, null, this);
            this.game.physics.arcade.collide(this.players[i].rightfoot, this.groundLayer);
            this.game.physics.arcade.collide(this.players[i], this.groundLayer);
            this.game.physics.arcade.collide(this.players[i], this.players[i].leftfoot);
            this.game.physics.arcade.collide(this.players[i], this.players[i].rightfoot);
            //check which way player is moving and do bounce animation
            if (this.players[i].motion == 1) {
                this.players[i].body.velocity.x = -(this.players[i].run ? this.players[i].speed : this.players[i].speed / 3);
                if (this.players[i].leftfoot.body.blocked.down || this.players[i].body.blocked.down) {
                    this.players[i].body.velocity.y = -100;
                }
                if (!this.players[i].leftHold) {
                    this.players[i].leftHold = true;
                    this.players[i].machete.scale.x *= -1;
                    this.players[i].machete.angle = 45;
                }
            }
            else if (this.players[i].motion == 2) {
                this.players[i].body.velocity.x = (this.players[i].run ? this.players[i].speed : this.players[i].speed / 3);
                if (this.players[i].leftfoot.body.blocked.down || this.players[i].body.blocked.down) {
                    this.players[i].body.velocity.y = -100;
                }
                if (this.players[i].body.velocity.x < 10) {
                    this.players[i].motion = 0;
                }
                if (this.players[i].leftHold) {
                    this.players[i].leftHold = false;
                    this.players[i].machete.scale.x *= -1;
                    this.players[i].machete.angle = -45;
                }
            }
            else {
                if (this.players[i].body.velocity.x > 5) {
                    this.players[i].body.velocity.x -= 5;
                }
                else if (this.players[i].body.velocity.x < -5) {
                    this.players[i].body.velocity.x += 5;
                }
                else {
                    this.players[i].body.velocity.x = 0;
                }
            }
            //blobs eating
            for (var j = 0; j < this.blobs.length; j++) {
                this.game.physics.arcade.overlap(this.players[i], this.blobs[j], function (player, blob) {
                    if (blob.tint != player.tint) {
                        var blobAud = this.add.audio('blob');
                        blobAud.play();
                        grow(player, 100);
                        this.blobs.splice(this.blobs.indexOf(blob), 1);
                        blob.destroy();
                    }
                }, null, this);
            }
            if (!this.players[i].fallThrough) {
                this.game.physics.arcade.collide(this.players[i].leftfoot, this.fallThroughPlatfromLayer);
                this.game.physics.arcade.collide(this.players[i].rightfoot, this.fallThroughPlatfromLayer);
                this.game.physics.arcade.collide(this.players[i], this.fallThroughPlatfromLayer);
            }
            if (i == 0) {
                var touchLadder = this.game.physics.arcade.overlap(this.players[0], this.objLadderLayer, function (player, ladder) {
                    if (this.currentMap == 3) {
                        this.players[0].body.checkCollision.up = false;
                    }
                    ladderMovement(this.pads[0], this.players[0]);
                }, null, this);
                if (!touchLadder) {
                    this.players[0].body.checkCollision.up = true;
                }
            }
            else if (this.numPlayers = 1 && i == 2) {
                this.players[0].body.allowGravity = true;
            }
            if (i == 1) {
                var touchLadder = this.game.physics.arcade.overlap(this.players[1], this.objLadderLayer, function (player, ladder) {
                    if (this.currentMap == 3) {
                        this.players[1].body.checkCollision.up = false;
                    }
                    ladderMovement(this.pads[1], this.players[1]);
                }, null, this);
                if (!touchLadder) {
                    this.players[1].body.checkCollision.up = true;
                }
            }
            else if (this.numPlayers > 1 && i == 2) {
                this.players[1].body.allowGravity = true;
            }
            if (i == 2) {
                var touchLadder = this.game.physics.arcade.overlap(this.players[2], this.objLadderLayer, function (player, ladder) {
                    if (this.currentMap == 3) {
                        this.players[2].body.checkCollision.up = false;
                    }
                    ladderMovement(this.pads[2], this.players[2]);
                }, null, this);
                if (!touchLadder) {
                    this.players[2].body.checkCollision.up = true;
                }
            }
            else if (this.numPlayers > 2 && i == 2) {
                this.players[2].body.allowGravity = true;
            }
            if (i == 3) {
                var touchLadder = this.game.physics.arcade.overlap(this.players[3], this.objLadderLayer, function (player, ladder) {
                    if (this.currentMap == 3) {
                        this.players[3].body.checkCollision.up = false;
                    }
                    ladderMovement(this.pads[3], this.players[3]);
                }, null, this);
                if (!touchLadder) {
                    this.players[3].body.checkCollision.up = true;
                }
            }
            else if (this.numPlayers > 3 && i == 3) {
                this.players[3].body.allowGravity = true;
            }
            //}, null, this);
            //for (var j = 0; j < this.tilesInLadder.length; j++) {
            //    console.log(this.game.physics.arcade.overlap(this.players[i], this.tilesInLadder[j]));
            //}
            //console.log(this.game.physics.arcade.overlap(this.players[i], this.ladderLayer));
            //console.log(this.ladderLayer.tiles);
            //this.players[i].body.checkCollision.down = !this.game.physics.arcade.overlap(this.players[i], this.fallThroughPlatfromLayer);
            //if (this.players[i].body.blocked.down && !this.players[i].fallThrough) {
            //this.game.physics.arcade.collide(this.players[i], this.fallThroughPlatfromLayer, function (player, platform) {
            //    if (this.pad1 && this.pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_Y) > 0.5) {
            //        this.players[i].body.checkCollision.down = !this.game.physics.arcade.overlap(this.players[i], this.fallThroughPlatfromLayer);
            //    }
            //},null,this);
            //}
            //PICK UP WEAPON
            this.game.physics.arcade.overlap(this.players[i], this.WorldWeapons, function (player, weapon) {
                if (!player.weapon && weapon.key != 'grenade' && weapon.key != 'mine') {
                    weapon.location.holdingWeapon = false;
                    weapon.scale.setTo(1, 1);
                    switch (weapon.key) {
                        case 'machete':
                            weapon.angle = 45;
                            if (!player.leftHold) {
                                weapon.scale.x *= -1;
                                weapon.angle = -45;
                            }
                            break;
                        case 'broom':
                            weapon.angle = 0;
                            if (!player.leftHold) {
                                weapon.scale.x *= -1;
                            }
                            break;
                        case 'knife':
                            weapon.scale.setTo(0.5, 0.5);
                            weapon.animations.play('hold');
                            if (!player.leftHold) {
                                weapon.scale.x *= -1;
                            }
                            break;
                    }
                    //weapon.angle = 45;
                    //if (!player.leftHold) {
                    //    weapon.scale.x *= -1;
                    //    weapon.angle = -45
                    //}
                    player.weapon = weapon;
                    this.WorldWeapons.remove(weapon, false);
                    this.world.add(weapon);
                }
                else if (weapon.key == 'grenade' && !player.grenade) {
                    weapon.location.holdingWeapon = false;
                    this.WorldWeapons.remove(weapon, false);
                    player.grenade = true;
                }
                else if (weapon.key == 'mine' && !player.mine) {
                    weapon.location.holdingWeapon = false;
                    this.WorldWeapons.remove(weapon, false);
                    player.mine = true;
                }
            }, null, this);
            //mine triggers
            for (var j = 0; j < this.mines.length; j++) {
                if (this.mines[j].data.live) {
                    this.game.physics.arcade.collide(this.players[i], this.mines[j], function (player, mine) {
                        addBoom(mine, this, this.mines);
                        console.log(this.explosions);
                    }, null, this);
                }
                //this.explosions.push(this.game.add.sprite(this.mines[j].world.x, this.mines[j].world.y, 'explosion'));
                //this.game.physics.arcade.enable(this.explosions[this.explosions.length - 1]);
                //this.explosions[this.explosions.length - 1].anchor.setTo(0.5, .85);
                //this.mines[j].destroy();
                //this.mines.splice(j, 1);
                //this.explosions[this.explosions.length - 1].animations.add('boommine'+j, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14], 30);
                //this.explosions[this.explosions.length - 1].animations.play('boommine' + j, 30, 0, true);
                //console.log(this.explosions);
                //this.explosions[this.explosions.length - 1].data.countdown = 300;
            }
            //players getting hurt by explosions
            this.game.physics.arcade.overlap(this.players[i], this.explosions, function (player, expl) {
                player.body.velocity.y = -200;
                player.body.velocity.x += player.world.x > expl.world.x ? 400 : -400;
                damage("expl", player, this);
            }, null, this);
        }
        this.WeaponSpawnLayer.forEach(function (child) {
            if (!child.holdingWeapon) {
                var weapon = undefined;
                var weaponNum = Math.floor(Math.random() * (6 - 1) + 1);
                //console.log('weapon ' + weaponNum + ' generated');
                switch (weaponNum) {
                    case 1:
                        weapon = this.WorldWeapons.create(child.x + 75, child.y, 'machete');
                        weapon.anchor.setTo(1, 0.5);
                        this.game.physics.arcade.enable(weapon);
                        break;
                    case 2:
                        weapon = this.WorldWeapons.create(child.x + 75, child.y, 'broom');
                        weapon.scale.setTo(0.5, 0.5);
                        weapon.angle = 90;
                        weapon.anchor.setTo(0.75, 0);
                        this.game.physics.arcade.enable(weapon);
                        break;
                    case 3:
                        weapon = this.WorldWeapons.create(child.x + 64, child.y, 'knife');
                        weapon.scale.setTo(0.5, 0.5);
                        weapon.anchor.setTo(1, 0.5);
                        weapon.animations.add('hold', [1], 1, true);
                        weapon.animations.add('stab', [2, 0], 20, false);
                        this.game.physics.arcade.enable(weapon);
                        break;
                    case 4:
                        weapon = this.WorldWeapons.create(child.x, child.y, 'grenade');
                        weapon.anchor.setTo(0.5, 1);
                        this.game.physics.arcade.enable(weapon);
                        break;
                    case 5:
                        weapon = this.WorldWeapons.create(child.x, child.y, 'mine');
                        weapon.anchor.setTo(0.5, 1);
                        this.game.physics.arcade.enable(weapon);
                        break;
                }
                child.holdingWeapon = true;
                weapon.location = child;
            }
        }, this);
    };
    SimpleGame.prototype.eatBlob = function (player, blob) {
        console.log(blob);
        player.area += 100;
        blob.destroy();
    };
    SimpleGame.prototype.render = function () {
        //for (var i = 0; i < 2; i++) {
        //    if (this.players[i].weapon) {
        //        this.game.debug.body(this.players[i].weapon);
        //    }
        //}
    };
    return SimpleGame;
}());
function makeBlob(player) {
    this.blobs.push(this.game.add.sprite(player.width / 2, player.height / 2, 'blob'));
    this.blobs[this.blobs.length - 1].scale.setTo(0.07, 0.07);
    this.blobs[this.blobs.length - 1].tint = player.tint;
    this.game.physics.arcade.enable(this.blobs[this.blobs.length - 1]);
    this.blobs[this.blobs.length - 1].body.gravity.y = 300;
    console.log(this.blobs);
}
//used to create ladders for boat mapp, will possibly be expanded for other maps
function createLadders(map, game) {
    //create sprite from objects retrieved from ladder object layer
    game.objLadderLayer = game.game.add.group();
    game.objLadderLayer.enableBody = true;
    var ladder = findObjectsByType('ladder', map, 'ObjLadderLayer');
    ladder.forEach(function (element) {
        createFromTiledObject(element, game.objLadderLayer, game);
    }, game);
}
function addBoom(trigger, game, array) {
    var kaboom = game.add.audio('explosion');
    kaboom.play();
    var explosion = game.explosions.create(trigger.world.x, trigger.world.y, 'explosion');
    explosion.anchor.setTo(0.5, .85);
    game.game.physics.arcade.enable(explosion);
    trigger.destroy();
    array.splice(array.indexOf(trigger), 1);
    explosion.animations.add('boom', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14], 30);
    explosion.animations.play('boom', 30, 0, true);
}
function createPlayerSpawns(map, game) {
    //create sprite from objects retrieved from ladder object layer
    game.PlayerSpawnLayer.enableBody = true;
    var pspawns = findObjectsByType('playerSpawn', map, 'PlayerSpawnLayer');
    //console.log(pspawns);
    //pspawns.forEach(function (element) {
    //    //createFromTiledObject(element, game.WeaponSpawnLayer, game);
    //    var sprite;
    //    //sprite = game.PlayerSpawnLayer.create(element.x, element.y, null);
    //    //Object.keys(element).forEach(function (key) {
    //    //    sprite[key] = element[key]
    //    //});
    //    //insert code to generate players here
    //}, game);
    return pspawns;
}
function createWeaponSpawns(map, game) {
    //create sprite from objects retrieved from ladder object layer
    game.WeaponSpawnLayer.enableBody = true;
    var wspawns = findObjectsByType('weaponSpawn', map, 'WeaponSpawnLayer');
    console.log(wspawns);
    wspawns.forEach(function (element) {
        //createFromTiledObject(element, game.WeaponSpawnLayer, game);
        var sprite;
        sprite = game.WeaponSpawnLayer.create(element.x, element.y, null);
        Object.keys(element).forEach(function (key) {
            sprite[key] = element[key];
        });
    }, game);
}
function bumpBody(player, foot) {
    player.body.velocity.y = -1;
}
//helper method of create ladders, might be expanded
function findObjectsByType(type, map, layer) {
    //find objects within object layer?
    var result = new Array();
    map.objects[layer].forEach(function (element) {
        if (element.type === type) {
            //Phaser uses top left, Tiled bottom left so we have to adjust
            //also keep in mind that the cup images are a bit smaller than the tile which is 16x16
            //so they might not be placed in the exact position as in Tiled
            element.y -= map.tileHeight;
            result.push(element);
        }
    }, this);
    return result;
}
//helper method of create ladders, might be expanded
function createFromTiledObject(element, group, game) {
    var sprite;
    if (element.x == 928) {
        sprite = game.objLadderLayer.create(element.x, element.y, 'BoatLadderLeft');
    }
    else if (element.x == 960) {
        sprite = game.objLadderLayer.create(element.x, element.y, 'BoatLadderRight');
    }
    //copy all properties to the sprite
    Object.keys(element).forEach(function (key) {
        sprite[key] = element[key];
    });
}
function createLaddersHouse(map, game) {
    //create sprite from objects retrieved from ladder object layer
    game.objLadderLayer = game.game.add.group();
    game.objLadderLayer.enableBody = true;
    var ladder = findObjectsByType('ladder', map, 'ObjLadderLayer');
    ladder.forEach(function (element) {
        var sprite = game.objLadderLayer.create(element.x, element.y, 'HouseLadder');
        //copy all properties to the sprite
        Object.keys(element).forEach(function (key) {
            sprite[key] = element[key];
        });
    }, game);
}
function createLaddersIndustrial(map, game) {
    //create sprite from objects retrieved from ladder object layer
    game.objLadderLayer = game.game.add.group();
    game.objLadderLayer.enableBody = true;
    var ladder = findObjectsByType('ladder', map, 'ObjLadderLayer');
    ladder.forEach(function (element) {
        var sprite = game.objLadderLayer.create(element.x, element.y, 'IndustrialLadder');
        //copy all properties to the sprite
        Object.keys(element).forEach(function (key) {
            sprite[key] = element[key];
        });
    }, game);
    var ladderTop = findObjectsByType('ladderTop', map, 'ObjLadderLayer');
    ladderTop.forEach(function (element) {
        var sprite = game.objLadderLayer.create(element.x, element.y, 'IndustrialLadderTop');
        //copy all properties to the sprite
        Object.keys(element).forEach(function (key) {
            sprite[key] = element[key];
        });
    }, game);
}
//used to have players move 
function ladderMovement(pad, player) {
    //could try and fuck around with these values more to get it to "flow" better
    if (pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_Y) > 0.5) {
        player.body.allowGravity = false;
        player.body.velocity.y = 150;
    }
    else if (pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_Y) < -0.5) {
        player.body.allowGravity = false;
        player.body.velocity.y = -150;
    }
    else {
        player.body.allowGravity = true;
    }
}
function addButtons(pad, players, playerIndex, game) {
    var player = players[playerIndex]; //get the player out of the array
    var newPlayers = new Array();
    //put other players into new array
    for (var i = 0; i < players.length; i++) {
        if (i != playerIndex) {
            newPlayers.push(players[i]);
        }
    }
    players = newPlayers;
    pad.getButton(Phaser.Gamepad.XBOX360_A).onDown.add(function () {
        if (player.jumpable) {
            player.body.velocity.y = -300;
            player.jumpable = false;
        }
        player.fallThrough = true;
    });
    pad.getButton(Phaser.Gamepad.XBOX360_A).onUp.add(function () {
        player.fallThrough = false;
    });
    pad.getButton(Phaser.Gamepad.XBOX360_B).onDown.add(function () {
    });
    pad.getButton(Phaser.Gamepad.XBOX360_LEFT_BUMPER).onDown.add(function () {
        //Throw a grenade
        if (player.grenade) {
            game.grenades.push(game.game.add.sprite(!player.leftHold ? player.lefthand.world.x : player.righthand.world.x, !player.leftHold ? player.lefthand.world.y : player.righthand.world.y, 'grenade'));
            game.game.physics.arcade.enable(game.grenades[game.grenades.length - 1]);
            game.grenades[game.grenades.length - 1].body.gravity.y = 300;
            game.grenades[game.grenades.length - 1].body.velocity.y = -((Math.random() * 400) + 100);
            game.grenades[game.grenades.length - 1].body.velocity.x = (player.leftHold ? -1 : 1) * ((Math.random() * 70) + 120) + (player.body.velocity.x);
            game.grenades[game.grenades.length - 1].body.bounce.setTo(0.4);
            game.grenades[game.grenades.length - 1].body.drag.setTo(0.2);
            game.grenades[game.grenades.length - 1].data.countdown = (Math.random() * 240) + 60;
            player.grenade = false;
        }
    });
    pad.getButton(Phaser.Gamepad.XBOX360_LEFT_TRIGGER).onDown.add(function () {
        //Throw a landmine
        if (player.mine) {
            game.mines.push(game.game.add.sprite(!player.leftHold ? player.lefthand.world.x : player.righthand.world.x, !player.leftHold ? player.lefthand.world.y : player.righthand.world.y, 'mine'));
            game.game.physics.arcade.enable(game.mines[game.mines.length - 1]);
            game.mines[game.mines.length - 1].anchor.setTo(0.5, 1);
            game.mines[game.mines.length - 1].body.gravity.y = 300;
            game.mines[game.mines.length - 1].body.velocity.y = -((Math.random() * 400) + 100);
            game.mines[game.mines.length - 1].body.velocity.x = (player.leftHold ? -1 : 1) * ((Math.random() * 20) + 200) + (player.body.velocity.x);
            game.mines[game.mines.length - 1].body.drag.setTo(0.2);
            game.mines[game.mines.length - 1].data.live = false;
            game.mines[game.mines.length - 1].data.boom = false;
            player.mine = false;
        }
    });
    pad.getButton(Phaser.Gamepad.XBOX360_X).onDown.add(function () {
        damage("self", player, game);
    });
    pad.getButton(Phaser.Gamepad.XBOX360_RIGHT_BUMPER).onDown.add(function () {
        if (player.weapon) {
            switch (player.weapon.key) {
                case 'machete':
                    var smack = game.add.audio('whoosh');
                    smack.play();
                    player.weapon.angle = 0;
                    break;
                case 'broom':
                    var smack = game.add.audio('whoosh');
                    smack.play();
                    if (player.leftHold) {
                        player.weapon.angle = 45;
                    }
                    else {
                        player.weapon.angle = -45;
                    }
                    break;
                case 'knife':
                    player.weapon.animations.play('stab');
                    break;
            }
            //player.weapon.angle = 0;
            for (var i = 0; i < players.length; i++) {
                if (game.physics.arcade.overlap(player.weapon, players[i])) {
                    damage(player.weapon.key, players[i], game);
                }
            }
        }
    });
    pad.getButton(Phaser.Gamepad.XBOX360_RIGHT_BUMPER).onUp.add(function () {
        if (player.weapon) {
            switch (player.weapon.key) {
                case 'machete':
                    player.weapon.angle = player.leftHold ? 45 : -45;
                    break;
                case 'broom':
                    player.weapon.angle = 0;
                    break;
                case 'knife':
                    player.weapon.animations.play('hold');
                    break;
            }
            //player.weapon.angle = player.leftHold ? 45 : -45;
        }
    });
    pad.getButton(Phaser.Gamepad.XBOX360_Y).onDown.add(function () {
        game.game.physics.arcade.overlap(player, game.WorldWeapons, function (player, weapon) {
            if (weapon.key != 'mine' && weapon.key != 'grenade') {
                player.weapon.destroy();
                player.weapon = undefined;
            }
        }, null, game);
        //if (player.weapon && game.game.) {
        //    player.weapon.destroy();
        //    player.weapon = undefined;
        //}
    });
}
function moveAxis(pad, player, game) {
    if (pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.1) {
        if (!player.leftHold) {
            player.leftHold = true;
            if (player.weapon) {
                player.weapon.scale.x *= -1;
                switch (player.weapon.key) {
                    case 'machete':
                        player.weapon.angle = 45;
                        break;
                }
                //player.weapon.angle = 45;
            }
        }
        if (pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > -0.8) {
            player.motion = 1;
            player.run = false;
        }
        if (pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.8) {
            player.motion = 1;
            player.run = true;
        }
    }
    else if (pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.1) {
        if (player.leftHold) {
            player.leftHold = false;
            if (player.weapon) {
                player.weapon.scale.x *= -1;
                switch (player.weapon.key) {
                    case 'machete':
                        player.weapon.angle = -45;
                        break;
                }
            }
        }
        if (pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < 0.8) {
            player.motion = 2;
            player.run = false;
        }
        if (pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.8) {
            player.motion = 2;
            player.run = true;
        }
    }
    else {
        player.motion = 0;
    }
    if (pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_Y) > 0.1) {
        player.fallThrough = true;
    }
    else {
        player.fallThrough = false;
    }
}
function grow(player, amount) {
    player.area += amount;
    if (player.area > 10000) {
        player.area = 10000;
    }
    player.scale.setTo(Math.sqrt(player.area / Math.PI) / 100, Math.sqrt(player.area / Math.PI) / 100);
    player.body.gravity.y = 350 - (player.area / 50);
}
function shrink(player, amount) {
    var prearea = player.area;
    player.area -= amount;
    if (player.area < 51) {
        player.area = 51;
    }
    player.scale.setTo(Math.sqrt(player.area / Math.PI) / 100, Math.sqrt(player.area / Math.PI) / 100);
    player.body.gravity.y = 350 - (player.area / 50);
    return prearea - player.area;
}
function damage(weapon, player, game) {
    console.log('hit ' + player.name);
    var amt = 0;
    switch (weapon) {
        case "machete":
            var amt = Math.ceil(shrink(player, 200) / 100) * 2; //THIS IS A MASS MULTIPLIER - ADJUST IN FINAL GAME
            break;
        case "broom":
            var amt = Math.ceil(shrink(player, 50) / 100) * 2;
            break;
        case "expl":
            var amt = 1;
            shrink(player, 50);
            break;
        case 'knife':
            var amt = Math.ceil(shrink(player, 300) / 100) * 2;
        case "self":
            if (shrink(player, 50) > 0) {
                var amt = 1;
            }
            break;
    }
    for (var i = 0; i < amt; i++) {
        var blob = game.add.audio('blob');
        blob.play();
        game.blobs.push(game.game.add.sprite(player.world.x, player.world.y - player.body.height, 'blob'));
        game.blobs[game.blobs.length - 1].scale.setTo(0.12, 0.12);
        game.blobs[game.blobs.length - 1].tint = player.tint;
        game.game.physics.arcade.enable(game.blobs[game.blobs.length - 1]);
        game.blobs[game.blobs.length - 1].body.gravity.y = 300;
        game.blobs[game.blobs.length - 1].body.velocity.y = -((Math.random() * 200) + 300);
        game.blobs[game.blobs.length - 1].body.velocity.x = (Math.random() * 400) - 200;
        game.blobs[game.blobs.length - 1].data.tintcount = 300;
    }
}
window.onload = function () {
    var game = new SimpleGame();
};
//function loadGame(level, players)
//{
//    levelGlob = level;
//    playersGlob = players;
//    var game = new SimpleGame();
//}  
//# sourceMappingURL=app.js.map