﻿class MeleeWeapon extends Weapon
{
    constructor(name: string, damage: number, durability: number)
    {
        super(name, damage, durability);
    }
}