var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var MeleeWeapon = (function (_super) {
    __extends(MeleeWeapon, _super);
    function MeleeWeapon(name, damage, durability) {
        return _super.call(this, name, damage, durability) || this;
    }
    return MeleeWeapon;
}(Weapon));
//# sourceMappingURL=MeleeWeapon.js.map