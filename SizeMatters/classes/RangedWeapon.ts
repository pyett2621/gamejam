﻿class RangedWeapon extends Weapon
{
    range: number;
    ammo: number;

    constructor(name: string, damage: number, range: number, ammo: number)
    {
        super(name, damage, ammo);
        this.range = range;
    }
}