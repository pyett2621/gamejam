﻿abstract class Weapon
{
    name: string;
    damage: number;
    durability: number;

    constructor(name: string, damage: number, durability: number)
    {
        this.name = name;
        this.damage = damage;
        this.durability = durability;
    }
}