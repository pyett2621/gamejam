var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var RangedWeapon = (function (_super) {
    __extends(RangedWeapon, _super);
    function RangedWeapon(name, damage, range, ammo) {
        var _this = _super.call(this, name, damage, ammo) || this;
        _this.range = range;
        return _this;
    }
    return RangedWeapon;
}(Weapon));
//# sourceMappingURL=RangedWeapon.js.map