var Weapon = (function () {
    function Weapon(name, damage, durability) {
        this.name = name;
        this.damage = damage;
        this.durability = durability;
    }
    return Weapon;
}());
//# sourceMappingURL=weapon.js.map