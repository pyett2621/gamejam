﻿class Player
{

    name: string;
    size: number;
    x: number;
    y: number;

    constructor(name: string, size: number)
    {
        this.size = size;
    }
}